; LockAndTurnOffScreen.ahk
;
; Locks the computer, turning off the screen (or screens) in the process.
; See: https://www.autohotkey.com/docs/commands/PostMessage.htm
; See: https://www.autohotkey.com/docs/misc/SendMessage.htm


; General configuration.
#SingleInstance force
#NoEnv
#NoTrayIcon


; `Win` + `L` does the trick.
#L::LockAndTurnOffScreen()


; Locks the computer and turns off the screen.
; These are the parameters for the `SendMessage` command:
;   0x112 is `WM_SYSCOMMAND`.
;   0xF170 is `SC_MONITORPOWER`.
;   2 means "turn the monitor off" (alternatives are -1 to turn the monitor on or 1 to activate monitor's low-power mode).
LockAndTurnOffScreen() {
    Sleep, 200
    DllCall("LockWorkStation")
    Sleep, 200
    SendMessage, 0x112, 0xF170, 2, , Program Manager
}